require iocStats,ae5d083
require calc,3.7.0+
require autosave,5.9.0+
require ess,0.0.1+
require caPutLog,3.6.0

# Set general environment variables
epicsEnvSet("IOC", "ioc-lab-test-wl-01")
epicsEnvSet("IOCNAME", "$(IOC)")
epicsEnvSet("TOP", ".")
# Set autosave environment variables
epicsEnvSet("AS_TOP", "$(TOP)/autosave")
# Set logging environment variables
epicsEnvSet("LOG_INET", "graylog-lab.cslab.esss.lu.se")
epicsEnvSet("LOG_INET_PORT", "9001")
epicsEnvSet("FACNAME", "labs-utg")
# Set access security environment variables
epicsEnvSet("ASG_PATH", "$(ess_DIR)")
epicsEnvSet("ASG_FILE", "unrestricted_access.asg")

iocshLoad("$(autosave_DIR)/autosave.iocsh")

iocshLoad("$(iocStats_DIR)/iocStats.iocsh")

iocshLoad("$(ess_DIR)/iocLog.iocsh")
iocshLoad("$(ess_DIR)/accessSecurityGroup.iocsh")

iocshLoad("$(caPutLog_DIR)/caPutLog.iocsh")

iocInit()

